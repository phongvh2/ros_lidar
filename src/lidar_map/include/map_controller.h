#ifndef MAP_SERVICE_H
#define MAP_SERVICE_H

#include <ros/ros.h>
#include <nav_msgs/MapMetaData.h>
#include <nav_msgs/GetMap.h>

namespace ohmnilabs {

class MapController
{
public:
  MapController(ros::NodeHandle &nh);
private:
  //  priavte variable
  ros::ServiceClient client_;
  nav_msgs::MapMetaData map_meta_data_;
  nav_msgs::OccupancyGrid map_data_;

  // defind subcribes
  ros::Subscriber map_sub_;
  ros::Subscriber map_metadata_sub_;

  // defind services
  ros::ServiceServer map_service_;
  // Private function
  void init(ros::NodeHandle &nh);
  void get_map_data_callback(nav_msgs::OccupancyGrid _map_data);
  void get_map_metadata_callback(nav_msgs::MapMetaData _map_metadata);
  bool request_map_data(nav_msgs::GetMap::Request &req, nav_msgs::GetMap::Response &res);
};

}


#endif // MAP_SERVICE_H
