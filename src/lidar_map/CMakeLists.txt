cmake_minimum_required(VERSION 2.8.3)
project(lidar_map)
find_package(catkin REQUIRED COMPONENTS
  roscpp
)
catkin_package(
#  INCLUDE_DIRS include
#  LIBRARIES get_map_data
#  CATKIN_DEPENDS roscpp
#  DEPENDS system_lib
)
include_directories(
  include
  ${catkin_INCLUDE_DIRS}
)
add_executable(
    map_node
    src/map_controller.cpp
    src/main_map_node.cpp
)
target_link_libraries(map_node
    ${catkin_LIBRARIES}
)
