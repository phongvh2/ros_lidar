#include "ros/ros.h"
#include "../include/map_controller.h"

int main(int argc, char **argv)
{
  ros::init(argc, argv, "call_map_service");
  ros::NodeHandle nh;
  ohmnilabs::MapController map_controller(nh);
  ros::spin();

  return 0;
}
