#include "../include/map_controller.h"

using namespace ohmnilabs;
using namespace std;

MapController::MapController(ros::NodeHandle &nh)
{
  this->init(nh);
}

void MapController::init(ros::NodeHandle &nh)
{
//  this->map_sub_ = nh.subscribe<nav_msgs::OccupancyGrid>("/map", 1, &MapController::get_map_data_callback, this);
//  this->map_metadata_sub_ = nh.subscribe<nav_msgs::MapMetaData>("/map_metadata", 1, &MapController::get_map_metadata_callback, this);
  client_ = nh.serviceClient<nav_msgs::GetMap>("static_map");
  ROS_INFO("Ready to provide the static map");
  this->map_service_ = nh.advertiseService("/dynamic_map", &MapController::request_map_data, this);
}

void MapController::get_map_data_callback(nav_msgs::OccupancyGrid _map_data)
{
  this->map_data_ = _map_data;
}

void MapController::get_map_metadata_callback(nav_msgs::MapMetaData _map_metadata)
{
  this->map_meta_data_ = _map_metadata;
}

bool MapController::request_map_data(nav_msgs::GetMap::Request &req, nav_msgs::GetMap::Response &res)
{
  nav_msgs::GetMap srv;
  if (client_.call(srv)) {

    res = srv.response;
  }
  return true;
}


