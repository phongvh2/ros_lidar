#include "lidar_localization.h"
using namespace ohmnilabs;

LidarLocalization::LidarLocalization(ros::NodeHandle &nh)
{
  this->init(nh);
  this->main_loop();
}

void LidarLocalization::init(ros::NodeHandle &nh) {
  printf("start 1");
  this->current_missing_location_ = 0;
  printf("start 2");
  this->amcl_subcriber_ = nh.subscribe("/amcl_pose", 1000, &LidarLocalization::amcl_pose_call_back, this);
  printf("start 3");
  this->global_localization_service_ = nh.serviceClient<std_srvs::Empty>("global_localization");
  printf("start 4");
}

void LidarLocalization::amcl_pose_call_back(geometry_msgs::PoseWithCovarianceStamped estimated_pose) {
  printf("estimate_pose");
  if (estimated_pose.pose.covariance[0] < AMCL_COVARIANCE_THRESHOLD) {
    this->current_missing_location_ ++;
  }

}

void LidarLocalization::main_loop() {
  while (true) {
    printf("mainloop\n");
    if (this->current_missing_location_ >= MAX_MISSING_LOCALIZATION) {
      this->current_missing_location_ = 0;
      std_srvs::Empty srv;
      global_localization_service_.call(srv);
    }
  }
}
