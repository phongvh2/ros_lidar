#include "lidar_localization.h"

int main(int argc, char **argv)
{
  ros::init(argc, argv, "lidar_localization_node");
  ros::NodeHandle nh;
  ohmnilabs::LidarLocalization lidarLocalization(nh);
  ros::spin();

  ROS_INFO("Hello world!");
}
