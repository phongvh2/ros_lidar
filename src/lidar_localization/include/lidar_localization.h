#ifndef LIDAR_LOCALIZATION_H
#define LIDAR_LOCALIZATION_H
#define AMCL_COVARIANCE_THRESHOLD 0.6
#define MAX_MISSING_LOCALIZATION 6
#include <ros/ros.h>
#include <geometry_msgs/PoseWithCovarianceStamped.h>
#include "std_srvs/Empty.h"

namespace ohmnilabs {

class LidarLocalization
{
public:
  LidarLocalization(ros::NodeHandle &nh);
private:
  int current_missing_location_;

  ros::Subscriber amcl_subcriber_;
  ros::ServiceClient global_localization_service_;

  void init(ros::NodeHandle &nh);
  void amcl_pose_call_back(geometry_msgs::PoseWithCovarianceStamped estimated_pose);
  void main_loop();
};

}
#endif // LIDAR_LOCALIZATION_H
