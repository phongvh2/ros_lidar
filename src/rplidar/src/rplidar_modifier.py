import rospy
from sensor_msgs.msg import LaserScan
import math

def modifyLidarCallback(data, publisher):
    data.angle_min = 15 * math.pi / 180 - math.pi
    data.angle_max = (359 - 15) * math.pi / 180 - math.pi
    publisher.publish(data)

def listener():
    rospy.init_node('lidar_modifier', anonymous=True)
    newLidarPublisher = rospy.Publisher("tb_scan", LaserScan, queue_size = 10)

    rospy.Subscriber("/scan", LaserScan, modifyLidarCallback, callback_args = newLidarPublisher)
    rospy.spin()

if __name__ == '__main__':

    try:
        listener()
    except rospy.ROSInterruptException:
        pass
