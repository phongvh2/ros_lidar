#include <ros/ros.h>
#include <std_msgs/String.h>
#include <sensor_msgs/LaserScan.h>

void chatterCallback(const sensor_msgs::LaserScan& msg)
{
//  ROS_INFO("I heard: [%s]", msg->data.c_str());
//  printf("angle = 0 distance: %f intensities: %f angle = 180: distance: %f intensities: %f\n", msg.ranges[0], msg.intensities[0], msg.ranges[180], msg.intensities[180]);
  printf("angle = 180: distance: %f intensities: %f\n", msg.ranges[180], msg.intensities[180]);
  printf("angle = 90: distance: %f intensities: %f\n", msg.ranges[90], msg.intensities[90]);
}

int main(int argc, char **argv)
{
  ros::init(argc, argv, "lidar_subcribe");
  ros::NodeHandle nh;

  ros::Subscriber sub = nh.subscribe("scan", 1000, chatterCallback);

  ros::spin();

  return 0;
}
