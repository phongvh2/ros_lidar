#!/usr/bin/env python

import os
import rospy
from nav_msgs.msg import OccupancyGrid
from cartographer_ros_msgs.srv import WriteState

class CartographerMapSaver(object):
    def __init__(self):
        self.enable = True

        rospy.wait_for_service('/write_state')
        self.map_saver = rospy.ServiceProxy('/write_state', WriteState)

        self.mapSubscriber = rospy.Subscriber("/map", OccupancyGrid, self.mapCallback)        

        self.file_path = "/app/ohmni_map.pbstream"

        self.is_saving = False
        
    def mapCallback(self, msg):
        if self.is_saving:
            return 

        self.is_saving = True

        response = self.map_saver("/app/ohmni_map.pbstream", False)
        rospy.loginfo("Write cartographer's map status: {}".format(response))

        self.is_saving = False
        
# Main function.
if __name__ == "__main__":
    # Initialize the node and name it.
    rospy.init_node("cartographer_map_saver")

    # Go to class functions that do all the heavy lifting.
    try:
        CartographerMapSaver()
    except rospy.ROSInterruptException:
        pass
    # Allow ROS to go to all callbacks.
    rospy.spin()
