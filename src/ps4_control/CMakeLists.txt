cmake_minimum_required(VERSION 2.8.3)
project(ps4_control)
find_package(catkin REQUIRED COMPONENTS
  roscpp
  geometry_msgs
)

catkin_package(
#  INCLUDE_DIRS include
#  LIBRARIES ps4_control
#  CATKIN_DEPENDS roscpp
#  DEPENDS system_lib
)

include_directories(
  include
  ${catkin_INCLUDE_DIRS}
)

 add_executable(${PROJECT_NAME}_node
     src/ps4_controller.cpp
     src/ps4_control.cpp
 )
## Specify libraries to link a library or executable target against
 target_link_libraries(${PROJECT_NAME}_node
   ${catkin_LIBRARIES}
 )
