#ifndef PS4_CONTROL_H
#define PS4_CONTROL_H
#include <ros/ros.h>
#include <geometry_msgs/Twist.h>
//#include <tf2_geometry_msgs/tf2_geometry_msgs.h>
namespace ohmnilabs {

class Ps4Control
{
private:
  ros::Publisher vel_publisher_;
  ros::Subscriber vel_subcriber_;

  void velocityCallback(geometry_msgs::Twist msg);

public:
  Ps4Control(ros::NodeHandle &nh);
};

}
#endif // PS4_CONTROL_H
