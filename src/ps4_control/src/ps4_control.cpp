#include "../include/ps4_control/ps4_control.h"


using namespace ohmnilabs;
using namespace std;

Ps4Control::Ps4Control(ros::NodeHandle &nh)
{
  vel_publisher_ = nh.advertise<geometry_msgs::Twist>("/tb_cmd_vel", 1000);
  vel_subcriber_ = nh.subscribe("/cmd_vel", 1000, &Ps4Control::velocityCallback, this);
}

void Ps4Control::velocityCallback(geometry_msgs::Twist msg) {
  vel_publisher_.publish(msg);
}
