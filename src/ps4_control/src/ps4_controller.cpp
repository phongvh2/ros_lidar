#include "../include/ps4_control/ps4_control.h"

int main(int argc, char **argv)
{
  ros::init(argc, argv, "ps4_controller");
  ros::NodeHandle nh;
  ohmnilabs::Ps4Control ps4Control(nh);
  ros::spin();

  return 0;
}
